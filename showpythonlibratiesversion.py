from pkg_resources import get_distribution

def main():
   
    libraries = [
        "numpy", "scipy", "pydub", "vosk", "langdetect", 
        "pytesseract", "python-docx", "nltk", "pandas", 
        "PyPDF2", "textract", "speechrecognition", 
        "PyMuPDF", "gensim", "pdfplumber", "numba", 
        "Pillow"  
    ]
    
    with open('requirements.txt', 'w') as f:
        for lib in libraries:
            try:
               
                version = get_distribution(lib).version
               
                f.write(f'{lib}=={version}\n')
            except Exception as e:
                
                print(f"Could not find version for library {lib}. Error: {str(e)}")

if __name__ == "__main__":
    main()
