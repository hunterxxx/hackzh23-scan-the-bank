from transformers import BertTokenizer, BertForTokenClassification
import torch
import re

# Load a pre-trained NER model and tokenizer
tokenizer = BertTokenizer.from_pretrained("dbmdz/bert-large-cased-finetuned-conll03-english")
model = BertForTokenClassification.from_pretrained("dbmdz/bert-large-cased-finetuned-conll03-english")

# Define your input data points
data_points = [
    "Agency less establish each\nPlayer with relationship group us often seek\nAssume successful side none hear fight similar",
    "Dark difficult catch drop note main\nDiscussion number art computer\nRock outside work board may Congress contain both",
    "Drug someone lawyer decide expect the head\nFew become reflect\n\nThrow question table before environmental skin\nWorry cultural say production",
    "Service name lot\nSpecific since drug fall join service",
    "Court person push score build father\nAssume since such word probably cut citizen view"
]

# Define your sensitive data patterns
sensitive_data_patterns = {
    "Direct": ["Full Name", "Email Address", "Company name", "RSA private key"],
    "Indirect": ["Address", "Phone number", "IBAN"],
    "Potential Indirect": ["Nationality", "Age", "Gender", "Professional qualification"]
}

# Define a regex pattern for IBANs
iban_pattern = r'\b(?:[A-Z]{2}\d{2}(?:\s?\d{4}){4}(?:\s?\d{1,4})?)\b'

def classify_entities(entities):
    for entity in entities:
        for category, patterns in sensitive_data_patterns.items():
            for pattern in patterns:
                if any(re.search(pattern, entity, re.IGNORECASE) for entity in entities):
                    return "TRUE"  # Found sensitive data, classify as TRUE
    return "FALSE"  # No sensitive data found, classify as FALSE

# Classify the input data points
for i, data_point in enumerate(data_points, start=1):
    # Tokenize the data point
    inputs = tokenizer(data_point, return_tensors="pt")

    # Perform NER
    with torch.no_grad():
        outputs = model(**inputs)

    predictions = outputs.logits.argmax(dim=2)

    # Map token labels back to words
    tokens = tokenizer.convert_ids_to_tokens(inputs["input_ids"][0].tolist())
    labels = [model.config.id2label[label_id] for label_id in predictions[0].tolist()]

    entities = []
    current_entity = []

    for token, label in zip(tokens, labels):
        if label.startswith("B-"):
            if current_entity:
                entities.append(" ".join(current_entity))
            current_entity = [token]
        elif label.startswith("I-"):
            current_entity.append(token)
        else:
            if current_entity:
                entities.append(" ".join(current_entity))
            current_entity = []

    if current_entity:
        entities.append(" ".join(current_entity))

    # Classify the entities
    classification = classify_entities(entities)

    print(f"Data Point {i}: {data_point}")
    print(f"Classification: {classification}\n")
