import os
from pathlib import Path
import pickle
from nltk.tokenize import word_tokenize
import nltk 
import speech_recognition as sr
import re
import csv
import subprocess
import shutil
import zipfile

##############FILE#########
# List of Python scripts to run
scripts_to_run = ['logtotxt.py', 'imagetotxt.py', 'pdftotxt.py', 'pubtotxt.py']

for script in scripts_to_run:
    try:
        # Execute the Python script using subprocess
        subprocess.run(['python3', script], check=True)
    except subprocess.CalledProcessError as e:
        print(f"Error running {script}: {e}")


source_folder = "../files"
destination_folder = "all_data"

# Define a list of file extensions to move
extensions_to_move = [".txt", ".log", ".md", ".html", ".xml", ".csv", ".ps1", ".py", ".db"]

# Create the destination folder if it doesn't exist
os.makedirs(destination_folder, exist_ok=True)

# Iterate through files in the source folder
for filename in os.listdir(source_folder):
    source_filepath = os.path.join(source_folder, filename)
    # Check if the file has one of the specified extensions
    if any(filename.lower().endswith(ext) for ext in extensions_to_move):
        # Move the file to the destination folder
        destination_filepath = os.path.join(destination_folder, filename)
        shutil.move(source_filepath, destination_filepath)
        #print(f"Moved {filename} to {destination_folder}")

##############FILE#########

#nltk.download('punkt')
recognizer = sr.Recognizer()

nltk.data.path.append('/')
docx_folder = "docx_files"

# Define patterns for sensitive data
sensitive_data_patterns = {
    "Direct": ["Full Name", "Email Address", "Company name", "RSA private key"],
    "Indirect": ["Address", "Phone number", "IBAN"],
    "Potential Indirect": ["Nationality", "Age", "Gender", "Professional qualification"]
}

# Define a regex pattern for IBANs
iban_pattern = r'\b(?:[A-Z]{2}\d{2}(?:\s?\d{4}){4}(?:\s?\d{1,4})?)\b'

def classify_zip(zip_file_path):
    # Define the target folder for extracted files
    target_folder = "files"

    # Create the target folder if it doesn't exist
    os.makedirs(target_folder, exist_ok=True)

    try:
        # Open and extract the contents of the zip file
        with zipfile.ZipFile(zip_file_path, 'r') as zip_ref:
            # If no password is provided, try to extract without a password
            zip_ref.extractall(target_folder)

        # Return "TRUE" to indicate that the zip file was successfully extracted
        return "TRUE"
    except zipfile.BadZipFile:
        # If the zip file is invalid (not password-protected), return "REVIEW"
        return "REVIEW"
    except Exception as e:
        # If there was an error during extraction, return "REVIEW"
        print(f"Error extracting zip file: {e}")
        return "REVIEW"

def save_dict_as_pickle(labels, filename):
    with open(filename, "wb") as handle:
        pickle.dump(labels, handle, protocol=pickle.HIGHEST_PROTOCOL)

def classifier(file_path):
    classification = "FALSE"  # Default to FALSE

    # Open the file to read out the content
    with open(file_path, 'r', encoding='latin-1') as file:
        content = file.read()

        # Tokenize the content into words
        words = word_tokenize(content)

        if content == "":
            classification = "FALSE"

        # Check for sensitive data patterns in the content
        for category, patterns in sensitive_data_patterns.items():
            # Initialize a flag to check if all required patterns for this category are found
            all_patterns_found = True
            for pattern in patterns:
                # Check if any of the patterns are not found in the content
                if all(re.search(pattern, word, re.IGNORECASE) for word in words):
                    all_patterns_found = False
                    break
            
            # If all required patterns for this category are found, classify as TRUE
            if all_patterns_found:
                classification = "TRUE"
                break

        # Check for IBAN patterns
        if re.search(iban_pattern, content):
            classification = "TRUE"  # Found IBAN, classify as TRUE 

    return classification
    
def main():
    # The path to your folder containing ZIP files
    ZIP_FILES_DIR = "../files"

    # Call the classify_zip function for each ZIP file in the directory
    for root, dirs, files in os.walk(ZIP_FILES_DIR):
        for file in files:
            if file.endswith(".zip"):
                zip_file_path = os.path.join(root, file)
                classification = classify_zip(zip_file_path)

    # Main code to process files
    input_folder = "all_data"  # Replace with your input folder path
    output_folder = "../results"  # Replace with your output folder path

    # Create the output folder if it doesn't exist
    os.makedirs(output_folder, exist_ok=True)

    # Create a list to store results
    results = []

    # Iterate through files in the input folder
    for root, dirs, files in os.walk(input_folder):
        for file_name in files:
            file_path = os.path.join(root, file_name)
            # Call the classification function
            classification = classifier(file_path)
            results.append({'filename': file_name, 'sensitive': classification})

    # Output results to result.csv
    output_csv = os.path.join(output_folder, 'crawler_labels.pkl')
    with open(output_csv, 'w', newline='') as csvfile:
        fieldnames = ['filename', 'sensitive']
        writer = csv.DictWriter(csvfile, fieldnames=fieldnames)

        writer.writeheader()
        for result in results:
            writer.writerow(result)

    print(f'Results saved to {output_csv}')

if __name__ == "__main__":
    main()
