import os
import fitz  # PyMuPDF
from PIL import Image
import pytesseract

# The path to your "files" folder
FILES_DIR = "../files"
TXT_OUTPUT_DIR = "all_data"

# Set a minimum word threshold to decide when to use OCR
MIN_WORD_THRESHOLD = 10

def main():
    """Main function to iterate over all PDF files, convert them to text, and save as individual .txt files."""
    
    # Create output directory if it doesn't exist
    if not os.path.exists(TXT_OUTPUT_DIR):
        os.makedirs(TXT_OUTPUT_DIR)
        
    # Walk through all the files in the specified directory
    for root, dirs, files in os.walk(FILES_DIR):
        for file in files:
            # Process only PDF files
            if file.endswith(".pdf"):
                pdf_path = os.path.join(root, file)
                pdf_reader = fitz.open(pdf_path)
                text_content = ""
                
                # Loop through all the pages in the PDF
                for page_num in range(len(pdf_reader)):
                    # Extract text using PyMuPDF
                    page_text = pdf_reader[page_num].get_text()
                    text_content += page_text
                    
                    # If the number of words is below the threshold, use OCR
                    if len(page_text.split()) < MIN_WORD_THRESHOLD:
                        # Convert the PDF page to an image
                        pix = pdf_reader[page_num].get_pixmap()
                        img = Image.frombytes("RGB", [pix.width, pix.height], pix.samples)
                        
                        # Use pytesseract to do OCR on the image
                        text_content += pytesseract.image_to_string(img)
                
                # Save the extracted text to a .txt file
                txt_filepath = os.path.join(TXT_OUTPUT_DIR, file)
                with open(txt_filepath, "w", encoding="utf-8") as txt_file:
                    txt_file.write(text_content)

if __name__ == "__main__":
    main()
