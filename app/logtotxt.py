import os

# Der Ordner, in dem Ihre .log Dateien gespeichert sind
FILES_DIR = "../files"
OUTPUT_DIR = "all_data"

def main():
    # Erstelle den Output-Ordner, falls er nicht existiert
    if not os.path.exists(OUTPUT_DIR):
        os.makedirs(OUTPUT_DIR)

    # Hole alle .log Dateien im FILES_DIR Ordner
    log_files = [f"{root}/{file}" for root, _, files in os.walk(FILES_DIR) for file in files if file.endswith(".log")]

    # Für jede .log Datei, kopiere den Inhalt in eine neue .txt Datei
    for log_file_path in log_files:
        # Setze den neuen Dateinamen (ersetze .log durch .txt und ändere den Speicherort)
        txt_file_path = f"{OUTPUT_DIR}/{os.path.basename(log_file_path).replace('.log', '.txt')}"
        
        # Öffne die .log Datei, lese den Inhalt und schreibe ihn in die .txt Datei
        with open(log_file_path, "r", encoding="utf-8") as log_file:
            content = log_file.read()
        
        with open(txt_file_path, "w", encoding="utf-8") as txt_file:
            txt_file.write(content)
        
        print(f"The content of {os.path.basename(log_file_path)} has been written to {os.path.basename(txt_file_path)}")

if __name__ == "__main__":
    main()
