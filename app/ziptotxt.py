import os
import zipfile

# The path to your folder containing ZIP files
ZIP_FILES_DIR = "zip_files"

def classify_zip(zip_file_path, password=None):
    """Classify a ZIP file based on extraction success or failure."""
    
    # Create the target folder if it doesn't exist
    os.makedirs(ZIP_FILES_DIR, exist_ok=True)

    try:
        # Open and extract the contents of the ZIP file
        with zipfile.ZipFile(zip_file_path, 'r') as zip_ref:
            if password:
                # If a password is provided, use it to extract the ZIP file
                zip_ref.extractall(ZIP_FILES_DIR, pwd=bytes(password, 'utf-8'))
            else:
                # If no password is provided, try to extract without a password
                zip_ref.extractall(ZIP_FILES_DIR)

        # Return "TRUE" to indicate that the ZIP file was successfully extracted
        return "TRUE"
    except zipfile.BadZipFile:
        # If the ZIP file is invalid (not password-protected), return "REVIEW"
        return "REVIEW"
    except Exception as e:
        # If there was an error during extraction, return "REVIEW"
        print(f"Error extracting ZIP file: {e}")
        return "REVIEW"

def main():
    """Main function to classify ZIP files in the specified directory."""
    
    # The path to your folder containing ZIP files
    ZIP_FILES_DIR = "zip_files"

    # Call the classify_zip function for each ZIP file in the directory
    for root, dirs, files in os.walk(ZIP_FILES_DIR):
        for file in files:
            if file.endswith(".zip"):
                zip_file_path = os.path.join(root, file)
                classification = classify_zip(zip_file_path)

                # Print the classification result
                print(f"Classification for {zip_file_path}: {classification}")

if __name__ == "__main__":
    main()
