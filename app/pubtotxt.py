import os

# The path to your "files" folder
FILES_DIR = "files"
TXT_OUTPUT_DIR = "pubtotxt"

def main():
    """Main function to iterate over all PUB files, and save them as individual .txt files."""
    
    # Create output directory if it doesn't exist
    if not os.path.exists(TXT_OUTPUT_DIR):
        os.makedirs(TXT_OUTPUT_DIR)

    # Walk through all files in the specified directory
    for root, dirs, files in os.walk(FILES_DIR):
        for file in files:
            # Process only PUB files
            if file.endswith(".pub"):
                pub_path = os.path.join(root, file)
                
                # Read the PUB file and extract text content
                try:
                    with open(pub_path, 'r', encoding='latin-1') as pub_file:
                        text_content = pub_file.read()
                        
                    # Save the extracted text to a .txt file
                    txt_filepath = os.path.join(TXT_OUTPUT_DIR, file.rsplit('.', 1)[0] + ".txt")
                    with open(txt_filepath, "w", encoding='utf-8', errors='replace') as txt_file:
                        txt_file.write(text_content)
                except Exception as e:
                    print(f"Could not convert {file} to text. Error: {e}")

if __name__ == "__main__":
    main()
