import os
from pydub import AudioSegment
import vosk
from langdetect import detect
from multiprocessing import Pool

# Directories for input files and output text files
FILES_DIR = "../files"
TXT_OUTPUT_DIR = "all_data"

# Paths to the Vosk language models
ENGLISH_LANGUAGE_MODEL_PATH = "Vosk_LanguageModell/vosk-model-small-en-us-0.15/"
GERMAN_LANGUAGE_MODEL_PATH = "Vosk_LanguageModell/vosk-model-small-de-0.15/"

def main():
    # Create output directory if it doesn't exist
    if not os.path.exists(TXT_OUTPUT_DIR):
        os.makedirs(TXT_OUTPUT_DIR)

    # Set Vosk log level to error only (to reduce console output)
    vosk.SetLogLevel(0)

    # Get all audio files from the input directory
    audio_files = [os.path.join(root, file) for root, _, files in os.walk(FILES_DIR) for file in files if file.endswith(".mp3")]

    # Create a multiprocessing pool and process all audio files
    with Pool() as p:
        p.map(process_audio_file, audio_files)

def process_audio_file(audio_path):
    # Initialize Vosk models (doing this in the function to avoid pickling issues)
    model_en = vosk.Model(ENGLISH_LANGUAGE_MODEL_PATH)
    model_de = vosk.Model(GERMAN_LANGUAGE_MODEL_PATH)

    # Load audio file and adjust its properties for processing
    audio = AudioSegment.from_mp3(audio_path)
    audio = audio.set_frame_rate(16000).set_channels(1)

    # Initialize Vosk recognizers with the respective language models
    recognizer_en = vosk.KaldiRecognizer(model_en, 16000)
    recognizer_de = vosk.KaldiRecognizer(model_de, 16000)

    # Process audio file in chunks and feed them to the recognizers
    for chunk in audio[::4000]:
        recognizer_en.AcceptWaveform(chunk.raw_data)
        recognizer_de.AcceptWaveform(chunk.raw_data)

    # Get the final recognition results
    result_en = eval(recognizer_en.FinalResult())
    result_de = eval(recognizer_de.FinalResult())

    # Detect the language of the recognized text (if any text was recognized)
    lang_en = detect(result_en['text']) if result_en['text'] else None
    lang_de = detect(result_de['text']) if result_de['text'] else None

    # Choose the best recognition result based on detected language and length of recognized text
    if lang_en == 'en' and lang_de != 'de':
        chosen_result = result_en['text']
    elif lang_de == 'de' and lang_en != 'en':
        chosen_result = result_de['text']
    else:
        chosen_result = result_en['text'] if len(result_en['text']) > len(result_de['text']) else result_de['text']

    # Write the chosen recognition result to a text file
    txt_filepath = os.path.join(TXT_OUTPUT_DIR, os.path.basename(audio_path).replace(".mp3", ".txt"))
    with open(txt_filepath, "w", encoding="utf-8") as txt_file:
        txt_file.write(chosen_result)

if __name__ == "__main__":
    main()
