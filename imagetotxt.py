import os
from PIL import Image
import pytesseract

# The path to your "files" folder
FILES_DIR = "files"
TXT_OUTPUT_DIR = "all_data"

def main():
    """Main function to iterate over all JPG and PNG files, convert them to text, and save as individual .txt files."""
    
    if not os.path.exists(TXT_OUTPUT_DIR):
        os.makedirs(TXT_OUTPUT_DIR)
        
    for root, dirs, files in os.walk(FILES_DIR):
        for file in files:
            if file.endswith((".jpg", ".png")):
                img_path = os.path.join(root, file)
                img = Image.open(img_path)
                
                # Extract text from the image using OCR
                text_content = pytesseract.image_to_string(img)
                
                txt_filepath = os.path.join(TXT_OUTPUT_DIR, file)
                with open(txt_filepath, "w", encoding="utf-8") as txt_file:
                    txt_file.write(text_content)

if __name__ == "__main__":
    main()
