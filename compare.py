import csv

# Read the contents of the two CSV files into dictionaries
file1_path = "labels.csv"  # Replace with the path to your first CSV file
file2_path = "results/result.csv"  # Replace with the path to your second CSV file

file1_data = {}
file2_data = {}

with open(file1_path, mode='r', newline='') as file1:
    reader = csv.reader(file1)
    next(reader)  # Skip the header
    for row in reader:
        file1_data[row[0]] = row[1]

with open(file2_path, mode='r', newline='') as file2:
    reader = csv.reader(file2)
    next(reader)  # Skip the header
    for row in reader:
        file2_data[row[0]] = row[1]

# Get a sorted list of file names from both files
sorted_files = sorted(set(file1_data.keys()) | set(file2_data.keys()))

# Compare the data and write differences to a new CSV file
diff_file_path = "diff.csv"

with open(diff_file_path, mode='w', newline='') as diff_file:
    fieldnames = ['File', 'Difference']
    writer = csv.DictWriter(diff_file, fieldnames=fieldnames)
    writer.writeheader()

    for file_name in sorted_files:
        classification1 = file1_data.get(file_name, None)
        classification2 = file2_data.get(file_name, None)

        if classification1 != classification2:
            difference = f'{classification1} -> {classification2}' if classification2 is not None else f'{classification1} -> N/A'
            writer.writerow({'File': file_name, 'Difference': difference})

print(f'Differences saved to {diff_file_path}')
