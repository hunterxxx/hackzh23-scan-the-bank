# Use Python 3.10 as the base image
FROM python:3.10

# Update the package list and install LLVM
RUN apt-get update && \
    apt-get install -y llvm && \
    # Clean up to reduce the image size
    apt-get clean && \
    rm -rf /var/lib/apt/lists/*

# Set the working directory
WORKDIR /app

# Copy the requirements file into the image
COPY app /app
COPY files /files
COPY Vosk_LanguageModell /Vosk_LanguageModell
COPY results /results

# Create a volume for the results directory
VOLUME /results

# Upgrade pip and install the necessary Python packages
RUN pip install --upgrade pip

# Installing numpy separately to leverage docker layer caching and speed up builds
#RUN pip install numpy
RUN pip install -r requirements.txt

# Download NLTK data
RUN python -c "import nltk; nltk.download('punkt')"

CMD ["python", "crawler.py"]