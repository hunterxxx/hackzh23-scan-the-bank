    + Access2Base - OutputTo / html Add suffix default value in file picker [Jean-Pierre Ledure]
    + Access2Base - OutputTo action - support encoding in csv files [Jean-Pierre Ledure]
    + Access2Base - OutputTo action more concise [Jean-Pierre Ledure]
TKA_1	ALASKA	TALKEETNA HELIPORT	afd	al_213_25OCT2007.png
ERROR info.magnolia.ai.text.TextClassificationModule 15.10.2020 00:12:42 -- Submission of text classification request has been failed at page path '/travel/about/careers/customer-experience-supervisor' with error code: 'null'.
*
* Linux System Utilities
*
acpid (ACPID) [N/y/?] n
blkdiscard (BLKDISCARD) [N/y/?] n
exec prefers applets (FEATURE_PREFER_APPLETS) [N/y/?] n
     Cr_sctns:            GheishaElastic: 0 eV  ---> 100 TeV
PROC_CREATOR_COUNT Be7 : neutronInelastic = 1
PROC_CREATOR_COUNT Be7 : protonInelastic = 120
PROC_CREATOR_COUNT Be8 : RadioactiveDecay = 1
PROC_CREATOR_COUNT Be8 : dInelastic = 5
PROC_CREATOR_COUNT Be9 : alphaInelastic = 1
./Era/Era%20-%20Vol.%201
 (/usr/local/texlive/2021/texmf-dist/tex/generic/infwarerr/infwarerr.sty
 (/usr/local/texlive/2021/texmf-dist/tex/latex/pgf/basiclayer/pgf.sty (/usr/local/texlive/2021/texmf-dist/tex/latex/pgf/utilities/pgfrcs.sty (/usr/local/texlive/2021/texmf-dist/tex/generic/pgf/utilities/pgfutil-common.tex
2021-10-26T21:51:55.696Z,66.696556,4231000,6 [FLog::GameJoinUtil] GameJoinUtil::makePlaceLauncherRequestForTeleport, response:
                Search End: 254450119.000 TAI (2008 024 // 12:35:19.00000 UTC)
      View Period Duration: 46944.000
ERROR info.magnolia.ai.text.TextClassificationModule 15.10.2020 00:12:42 -- Submission of text classification request has been failed at page path '/bdcwebsite/investor-relations/board-of-directors/hazem-hegazy' with error code: 'null'.
ERROR info.magnolia.aws.foundation.AwsCredentialsProvider 15.10.2020 00:12:42 -- AWS credentials are expected to be set in Password manager module.
ERROR info.magnolia.ai.text.TextClassificationModule 15.10.2020 00:12:42 -- Submission of text classification request has been failed at page path '/bdcwebsite/investor-relations/corporate-governance' with error code: 'null'.
ERROR info.magnolia.aws.foundation.AwsCredentialsProvider 15.10.2020 00:12:42 -- AWS credentials are expected to be set in Password manager module.
ERROR info.magnolia.ai.text.TextClassificationModule 15.10.2020 00:12:42 -- Submission of text classification request has been failed at page path '/bdcwebsite/investor-relations/board-of-directors/hisham-sanad' with error code: 'null'.

    + ...and this looks like a memory leak, too [Stephan Bergmann]
    + ...one more detail to get things fixed [Stephan Bergmann]
    + ...these args are all default ones [Stephan Bergmann]
    + .doc import image flipped status (tdf#56321) [Justin Luth]
    + .doc: emulate table keep-with-next paragraph (tdf#91083) [Justin Luth]
                                  1993-061D      Healthsat 2                    Healthsat 2               S22826
                                  1993-061E      ITAMsat                        ITAMsat                   S22827
[     47641] [    GTAProcess]  UV loop: httpClient/ 
LaTeX Font Info:    Font shape `U/msa/m/n' will be
(Font)              scaled to size 10.42007pt on input line 3.
    **************************************************[0m
    [0m START THE BEHAVIORAL CARTOGRAPHY ALGORITHM[0m
tdf#106544 Calc: Merge cells dialog is difficult to understand for new/infrequent users [heiko tietze]
tdf#106611 EDITING: click on status bar does not change the overwrite mode [Jim Raykowski]
tdf#106667 Hang when trying to open a dialog and the whole sheet is selected [Noel Grandin]
tdf#106746 copy/pasting revisions copy deleted words [Aron Budea, Zdeněk Crhonek]
tdf#106991 Highlighting remains after select and no fill. [Tamás Zolnai]
tdf#107026 Adding page resulting from inserting text doesn't update document view. [Mert Tümer]
tdf#107079 LOCALHELP: Index: colors;defining and saving needs update to new color tab [Olivier Hallot]
tdf#107501 Convert manual tests to automated UI tests [Saurav Chirania]
tdf#107555 Apply the 'Default Style' table style to newly inserted tables [Jim Raykowski]
tdf#107567 Text previews in Paragraph dialog not showing correctly for RTL [Jim Raykowski]
tdf#107601 Rename Data > Statistics > "t-test" to "paired t-test" [Olivier Hallot]
tdf#107608 FILEOPEN PPTX: pattern fill has transparent background instead of white background [Tamás Zolnai]
tdf#107866 Changing table column width/row height with Alt+arrow key strokes is broken [Noel Grandin]
tdf#108048 Change numbering pages with new style not working when set from within the macro [Mike Kaganski]
tdf#108122 Copy/paste of mixed content (text and not embedded images) does not paste the images [Miklos Vajna]
tdf#108124 CRASH: bad dynamic_cast! after redo operation [Zdeněk Crhonek]
tdf#108210 Can't open file for editing if there is a lingering lock file from a different user [Mike Kaganski]
tdf#108224 Insert -> Field -> Author puts "DocInformation:Created" field instead of "Author" [Gabriele Ponzo]
tdf#108446 Android: Shape is not changed after change [Mert Tümer]
tdf#108448 FILTER:DOC endnotes page gains an extra paragraph each roundtrip [Justin Luth]
tdf#108523 Remove @author annotations from source code [Manuj Vashist]
tdf#108556 Start center welcome text should respect window width [heiko tietze]
tdf#108580 Cannot run LibreOffice, api-ms-win-crt-runtime-l1-1-0.dll is missing (for local solution see comment 7) [Mike Kaganski]
tdf#108608 EDITING: Draw file be unresponsive if a large text is pasted into a textbox [Noel Grandin]
tdf#108894 Update Office version in file format confirmation dialog to not refer to Office 2013 [Adolfo Jayme Barrientos]
tdf#113621 FILESAVE XLSX Conditional formatting range is lost if it is applied to a whole column [Markus Mohrhard]
tdf#113624 Wrong position and size of the characters in EMF/EMF+ dual mode [Bartosz Kosiorek]
tdf#113651 UI Edit - Track Changes submenu item names inconsistent with ones in Writer [Yousuf Philips]
tdf#113688 Cannot set data series filling to pattern [Katarina Behrens]
tdf#113715 Customize dialog: .uno:EditAnnotation visible in functions list [Maxim Monastirsky]
Package pdftex.def Info: images/multiplex/Multilayer_NY_op2.png  used on input line 17.
LogPluginManager: Mounting Engine plugin ChaosEditor
ERROR info.magnolia.aws.foundation.AwsCredentialsProvider 15.10.2020 00:12:42 -- AWS credentials are expected to be set in Password manager module.
ERROR info.magnolia.ai.text.TextClassificationModule 15.10.2020 00:12:42 -- Submission of text classification request has been failed at page path '/sportstation/about' with error code: 'null'.
-- [download 52% complete]
-- [download 53% complete]
       Additional Information : (multiple lines)

     Site Name                : Washington County
     Four Character ID        : OHWA
     Monument Inscription     :
     IERS DOMES Number        : (A9)
     CDP Number               : (A4)
<weise> Anyone down there who can tell us what it's like?
<DrStupid> KNBC - Clinton and Gore are addressing the people.
<DrStupid> KNBC - Gore - blah blah blah
<DrStupid> KNBC - Gore - there will be Martin Luther King ceremonies later
<KNBC> jesus gore shut the hell up
<DrStupid> KNBC - Gore - blah blah blah intro intro intro blah blah blah
<DrStupid> KNBC - Gore - senator represnetative state party blah 
<tasm> what the hell is Gore saying
(YaZoO/#earthquake) attak : yeh
(WildEep/#earthquake) Please "Cal Tech" is Caltech, all one word.
<09:16AM DrStupid> I just emailed gore telling him he's an idiot.
(KD4CQY/#earthquake) You earned your nick
<09:16AM DrStupid> :)
<09:16AM NeilM> BBC LAYEST
*** Netsplit at 09:16:43 (Uni-Erlangen.DE fu-berlin.de)
<09:16AM tasm> CBS - church raised off foundation
<09:16AM NeilM> 25 Feared Dear, 100 injured, quake lasted for 60s and was felt as far away as pasenda STATE OF EMERGENCY DECLAREDE
<09:17AM NeilM> 100s
<09:17AM tasm> neil: quake lasted 30-45 sec
<09:17AM AttackYak> KNBC: Also at Northridge Mall, parts of Bullocks collapsed - Would have killed lots if open (I know I used to shop there)
<09:17AM DrStupid> KNBC - the Kaiser Permanente building is NOT a care center.
(WildEep/#earthquake) sg joherius hope everything is ok -- im sure it will be.
<09:17AM DrStupid> KNBC - it is a doctors office facility
<09:18AM NeilM> BBC REPORT==RUETERS report
<09:18AM DrStupid> KNBC - Grenada Hills hosptical emergency and serious injury care now CLOSED
<09:18AM tasm> CBS: the president is still "hjholding " to his MLK day and some freaking "new program"
<09:18AM DrStupid> KNBC - Grenada Hills - light injuries only
(YaZoO/#earthquake) bbc sucks shit.. they aint showing nothing
<09:18AM tasm> ()$*#)(#$* the program.. i want to hear what the president has to say abotu the erthquake
<09:18AM AttackYak> KNBC - Sections of 118 west-bound at havenhearst collapsed - 1 car hit section after it fell - Driver Status unknown
