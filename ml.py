import os
from docx import Document
import re
import csv
import nltk
import pandas as pd
import PyPDF2
import textract
from nltk.tokenize import word_tokenize
from pydub import AudioSegment
import speech_recognition as sr
import zipfile

#nltk.download('punkt')
recognizer = sr.Recognizer()

nltk.data.path.append('/')
docx_folder = "docx_files"

# Define patterns for sensitive data
sensitive_data_patterns = {
    "Direct": ["Full Name", "Email Address", "Company name", "RSA private key"],
    "Indirect": ["Address", "Phone number", "IBAN"],
    "Potential Indirect": ["Nationality", "Age", "Gender", "Professional qualification"]
}

# Define a regex pattern for IBANs
iban_pattern = r'\b(?:[A-Z]{2}\d{2}(?:\s?\d{4}){4}(?:\s?\d{1,4})?)\b'

# Define a function to classify a file
def classify_file(file_path):
    # Determine the file type based on its extension
    file_extension = os.path.splitext(file_path)[-1].lower()

    # Initialize the classification result
    classification = "REVIEW"  # Default to REVIEW

    # Process different file types
    if file_extension == '.txt':
        with open(file_path, 'r', encoding='utf-8') as file:
            content = file.read()
            # Implement text-based classification logic here
            classification = classify_text(content)

    elif file_extension == '.xlsx':
        # Use Pandas to read Excel files (.xlsx) and analyze data
        df = pd.read_excel(file_path)
        # Implement Excel file data analysis logic here
        classification = classify_excel(df)

    elif file_extension == '.pdf':
        # Use PyPDF2 to extract text from PDF files
        pdf_text = extract_text_from_pdf(file_path)
        # Implement PDF text analysis logic here
        classification = classify_text(pdf_text)

    elif file_extension == '.zip':
        # Handle MP3 files
        classification = classify_zip(file_path)
    
    elif file_extension == '.pem':
        # Handle pem files
        classification = "TRUE"

    # Add more file type handlers for audio, images, etc.
    elif file_extension == '.docx':
        classification = convert_docx_to_txt(file_path)
        
    elif file_extension == '.mp3':
        # Handle MP3 files
        #classification = classify_mp3(file_path)
        123
    return classification

def classify_text(content):
    # Implement text-based classification logic here
    classification = "REVIEW"  # Default to REVIEW

    # Tokenize the content into words
    words = word_tokenize(content)

    # Check for sensitive data patterns in the content
    for category, patterns in sensitive_data_patterns.items():
        for pattern in patterns:
            if any(re.search(pattern, word, re.IGNORECASE) for word in words):
                classification = "FALSE"  # Found sensitive data, classify as FALSE
                break

    # Check for IBAN patterns
    if re.search(iban_pattern, content):
        classification = "FALSE"  # Found IBAN, classify as FALSE

    return classification

def contains_sensitive_data(text):
    # Check for sensitive data patterns in the text
    for category, patterns in sensitive_data_patterns.items():
        for pattern in patterns:
            if re.search(pattern, text, re.IGNORECASE):
                return True
    
    # Check for IBAN patterns in the text
    if re.search(iban_pattern, text):
        return True

    return False

# Define a function to convert DOCX to TXT
def convert_docx_to_txt(file_path):
    doc = Document(file_path)
    txt_content = ""
    for paragraph in doc.paragraphs:
        txt_content += paragraph.text + "\n"
    return txt_content

# Create a list to store the converted text
converted_text_list = []

# Iterate through DOCX files in the folder
for root, dirs, files in os.walk("files"):
    for file_name in files:
        if file_name.endswith('.docx'):
            file_path = os.path.join(root, file_name)
            converted_text = convert_docx_to_txt(file_path)
            converted_text_list.append((file_name, converted_text))

# Optionally, save the converted text to TXT files
docx_output_folder = "docx_files"
os.makedirs(docx_output_folder, exist_ok=True)

for file_name, converted_text in converted_text_list:
    docx_output_path = os.path.join(docx_output_folder, file_name)
    with open(docx_output_path, 'w', encoding='utf-8') as docx_file:
        docx_file.write(converted_text)
#end docx

def classify_zip(zip_file_path, password=None):
    # Define the target folder for extracted files
    target_folder = "zip_files"

    # Create the target folder if it doesn't exist
    os.makedirs(target_folder, exist_ok=True)

    try:
        # Open and extract the contents of the zip file
        with zipfile.ZipFile(zip_file_path, 'r') as zip_ref:
            if password:
                # If a password is provided, use it to extract the zip file
                zip_ref.extractall(target_folder, pwd=bytes(password, 'utf-8'))
            else:
                # If no password is provided, try to extract without a password
                zip_ref.extractall(target_folder)

        # Return "TRUE" to indicate that the zip file was successfully extracted
        return "TRUE"
    except zipfile.BadZipFile:
        # If the zip file is invalid (not password-protected), return "REVIEW"
        return "REVIEW"
    except Exception as e:
        # If there was an error during extraction, return "REVIEW"
        print(f"Error extracting zip file: {e}")
        return "REVIEW"

# Define a function to classify Excel data
def classify_excel(df, sensitive_data_patterns, iban_pattern):
    # Implement Excel data analysis logic here
    classification = "REVIEW"  # Default to REVIEW

    # Iterate through columns and rows to check for sensitive data
    for column in df.columns:
        for index, cell_value in enumerate(df[column]):
            # Check for sensitive data patterns in cell values
            for category, patterns in sensitive_data_patterns.items():
                for pattern in patterns:
                    if re.search(pattern, str(cell_value), re.IGNORECASE):
                        classification = "FALSE"  # Found sensitive data, classify as FALSE
                        break

            # Check for IBAN patterns in cell values
            if re.search(iban_pattern, str(cell_value)):
                classification = "FALSE"  # Found IBAN, classify as FALSE

            # You can add more specific checks based on your Excel data structure

    return classification

# Define a function to extract text from PDF files using PyPDF2
def extract_text_from_pdf(pdf_path):
    text = ""
    try:
        pdf_file = open(pdf_path, 'rb')
        pdf_reader = PyPDF2.PdfReader(pdf_file)
        for page_num in range(len(pdf_reader.pages)):
            page = pdf_reader.pages[page_num]
            text += page.extract_text()
        pdf_file.close()
    except Exception as e:
        print(f"Error extracting text from PDF: {e}")
    return text

# Main code to process files
input_folder = "files"  # Replace with your input folder path
output_folder = "results"  # Replace with your output folder path

# Create the output folder if it doesn't exist
os.makedirs(output_folder, exist_ok=True)

# Create a list to store results
results = []

# Iterate through files in the input folder
for root, dirs, files in os.walk(input_folder):
    for file_name in files:
        file_path = os.path.join(root, file_name)
        # Call the classification function
        classification = classify_file(file_path)
        results.append({'File': file_name, 'Classification': classification})

# Output results to result.csv
output_csv = os.path.join(output_folder, 'result.csv')
with open(output_csv, 'w', newline='') as csvfile:
    fieldnames = ['File', 'Classification']
    writer = csv.DictWriter(csvfile, fieldnames=fieldnames)

    writer.writeheader()
    for result in results:
        writer.writerow(result)

print(f'Results saved to {output_csv}')