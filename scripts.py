import subprocess

# List of Python scripts to run
scripts_to_run = ['logtotxt.py', 'imagetotxt.py', 'pdftotxt.py, pubtotxt.py']

for script in scripts_to_run:
    try:
        # Execute the Python script using subprocess
        subprocess.run(['python3', script], check=True)
    except subprocess.CalledProcessError as e:
        print(f"Error running {script}: {e}")