import os

# Define the project directory
project_directory = '/Users/work/Desktop/hunter/hackzh23-scan-the-bank'  # Replace with the actual path

# Create a dictionary to store file type counts
file_counts = {}

# Iterate through the files directory
files_directory = os.path.join(project_directory, 'files')
for root, dirs, files in os.walk(files_directory):
    for file in files:
        # Get the file extension
        file_extension = os.path.splitext(file)[1].lower()
        
        # Update the file type count
        file_counts[file_extension] = file_counts.get(file_extension, 0) + 1

# Create the results directory if it doesn't exist
results_directory = os.path.join(project_directory, 'results')
os.makedirs(results_directory, exist_ok=True)

# Write the file type counts to a text file
results_file_path = os.path.join(results_directory, 'file_counts.txt')
with open(results_file_path, 'w') as results_file:
    for file_extension, count in file_counts.items():
        results_file.write(f'{file_extension}: {count}\n')

print("File counts have been saved to 'file_counts.txt' in the 'results' directory.")
